//Pendientes ID y userID

import './App.css';
import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';
import 'styled-components';


const P5 = () => {
  const [todos, setTodos] = useState([]);

  const fetchTodos = async () => {
    try {
      const response = await fetch('http://jsonplaceholder.typicode.com/todos');
      const data = await response.json();
      setTodos(data);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    fetchTodos();
  }, []);

  const columns = [
    {
      name: 'ID',
      selector: row => row.id,
    },

    {
        name: 'UserID',
        selector: row => row.userId,
      },

  ];

  return (
    <div>
      <h1> ID e User ID de los pendientes </h1>
      <DataTable columns={columns} data={todos} pagination />
    </div>
  );
};

export default P5;