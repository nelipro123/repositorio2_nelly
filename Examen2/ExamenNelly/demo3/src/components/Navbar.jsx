
//Lista de funciones  para Menu
import ListGroup from 'react-bootstrap/ListGroup';
import '../Navbar.css';
import React, { useState } from 'react';


//LA lista de lo que se espera tenga el menu
const ListaOpciones = () => {
  const [items] = useState([
    'Pendientes (Solo ID)',
    'Pendientes (IDs y Titles)',
    'Pendientes sin resolver (ID y Title)',
    'Pendientes resueltos (ID y title)',
    'Pendientes (ID y user ID)',
    'Pendientes resueltos (ID y userID)',
   ' Pendientes sin resolver (ID y UserID)',
    ]);


    //Boton que debia ser interactivo pero colapse en el proceso y no supoe hacer routers
  return (
    <div>
      <h1>Lista de opciones </h1>
      <ListGroup>
        {items.map((item, index) => (
          <ListGroup.Item key={index}>
            {item}
            <div class="d-grid gap-2 d-md-block">
            <button class="btn btn-primary" href="#" role="button"> ir </button>
            </div>

          </ListGroup.Item>
        ))}
      </ListGroup>
    </div>
  );
};

export default ListaOpciones;