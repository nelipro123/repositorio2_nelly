//Pendientes resueltos ID y Titulo

import './App.css';
import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';
import axios from 'axios';

function P4() {
  const [completeTodos, setcompleteTodos] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get('https://jsonplaceholder.typicode.com/todos');
      const completeData = response.data.filter(todo => !todo.completed);
      setcompleteTodos(completeData);
    } catch (error) {
      console.error('Error al obtener datos:', error);
    }
  };

  const columns = [
    {
      name: 'ID',
      selector: 'id',
      sortable: true,
    },
    {
      name: 'Título',
      selector: 'title',
      sortable: true,
    },
  ];

  return (
    <div>
      <h1>ID y Title de pendientes resueltos </h1>
      <DataTable columns={columns} data={completeTodos} pagination highlightOnHover />
    </div>
  );
}

export default P4;