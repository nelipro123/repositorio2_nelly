import './App.css';
import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';
import Navbar from './components/Navbar';
import 'styled-components';


const App = () => {
  const [todos, setTodos] = useState([]);

  const fetchTodos = async () => {
    try {
      const response = await fetch('http://jsonplaceholder.typicode.com/todos');
      const data = await response.json();
      setTodos(data);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    fetchTodos();
  }, []);

  const columns = [
    {
      name: 'ID',
      selector: row => row.id,
    },
    {
      name: 'Título',
      selector: row => row.title,
    },
    {
      name: 'Completado',
      selector: row => (row.completed ? 'Sí' : 'No'),
    },
    {
      name: 'UserID',
      selector: row => row.userId,
    },
  ];

  return (
    <div>
      <Navbar />
      <h1>Lista de Tareas</h1>
      <DataTable columns={columns} data={todos} pagination />
    </div>
  );
};

export default App;