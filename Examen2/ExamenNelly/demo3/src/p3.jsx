//Pendientes sin resolver ID y Title

import './App.css';
import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';
import axios from 'axios';

function P3() {
  const [incompleteTodos, setIncompleteTodos] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get('https://jsonplaceholder.typicode.com/todos');
      const incompleteData = response.data.filter(todo => !todo.completed);
      setIncompleteTodos(incompleteData);
    } catch (error) {
      console.error('Error al obtener datos:', error);
    }
  };

  const columns = [
    {
      name: 'ID',
      selector: 'id',
      sortable: false,
    },
    {
      name: 'Título',
      selector: 'title',
      sortable: false,
    },
  ];

  return (
    <div>
      <h1>ID y Title de pendientes sin resolver</h1>
      <DataTable columns={columns} data={incompleteTodos} pagination highlightOnHover />
    </div>
  );
}

export default P3;