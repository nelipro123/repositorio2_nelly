import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import '../Header.css';

function Header() {
  return (
    <Navbar className="navbar"> 
      <Container>
        <Navbar.Brand href="#home">Barrita</Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link href="#inicio">Inicio</Nav.Link>
          <Nav.Link href="#conocenos">Conocenos</Nav.Link>
          <Nav.Link href="#hola">Hola</Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
}

export default Header;