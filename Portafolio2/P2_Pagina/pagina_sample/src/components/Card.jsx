import React from 'react';
import Card from 'react-bootstrap/Card';
import CardGroup from 'react-bootstrap/CardGroup'; 

//IMportacion de imagenes a utilizar en la galeria 
import Ame1 from '../assets/images/Ame1.jpg'; 
import Ame2 from '../assets/images/Ame2.jpg';
import Ame3 from '../assets/images/Ame3.jpg'; 
import Ame4 from '../assets/images/Ame4.jpg'; 


//Declaracion de funcion 
function Galeria() {
  return (
//Se crea el primer Card
///En card Title se asigna un titulo a la Card, mientras en text el cuerpo de esta
    <CardGroup>
      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src={Ame1} />
        <Card.Body>
          <Card.Title> Normal  </Card.Title>
          <Card.Text>
          :3
          </Card.Text>
        </Card.Body>
      </Card>

      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src={Ame2} />
        <Card.Body>
          <Card.Title> Sorpresa  </Card.Title>
          <Card.Text>
            :o
          </Card.Text>
        </Card.Body>
      </Card>


      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src={Ame3} />
        <Card.Body>
          <Card.Title> Triste  </Card.Title>
          <Card.Text>
            :c
          </Card.Text>
        </Card.Body>
      </Card>


      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src={Ame4} />
        <Card.Body>
          <Card.Title> LOkura </Card.Title>
          <Card.Text>
            uwu
          </Card.Text>
        </Card.Body>
      </Card>
    </CardGroup>
  );
}

export default Galeria;