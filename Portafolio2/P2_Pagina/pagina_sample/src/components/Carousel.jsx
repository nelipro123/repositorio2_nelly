/* import React from 'react';
import { Carousel } from 'react-bootstrap';

const ImageCarousel = () => {
  return (
    <Carousel>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="../assets/images/KAngel.jpg"
          alt="KANgel selfie"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="../assets/images/KAngel2.jpg"
          alt="KAngel corazon"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="../assets/images/KAngel3.jpg"
          alt="KAngel selfie 2"
        />
      </Carousel.Item>
    </Carousel>
  );
};

export default ImageCarousel;

*/

import React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import { Image } from 'react-bootstrap';
// Se importan las imágenes
import KAngel from '../assets/images/KAngel.jpg';
import KAngel2 from '../assets/images/KAngel2.jpg';
import KAngel3 from '../assets/images/KAngel3.jpg';

function ImageCarousel() {
  return (
    <Carousel>
      <Carousel.Item>
        <Image src={KAngel} className="d-block w-100" />
      </Carousel.Item>
      <Carousel.Item>
        <Image src={KAngel2} className="d-block w-100" />
      </Carousel.Item>
      <Carousel.Item>
        <Image src={KAngel3} className="d-block w-100" />
      </Carousel.Item>
    </Carousel>
  );
}

export default ImageCarousel;

