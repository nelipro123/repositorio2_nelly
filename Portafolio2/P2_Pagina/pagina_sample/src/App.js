import './App.css';
import Header from './components/Header'
import Carousel from './components/Carousel';
import Card from './components/Card';


function App() {
  return (
    <div>
      <Header/>
      <Carousel/>
      <Card/>
    </div>

  );
}

export default App;
