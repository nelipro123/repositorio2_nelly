
import './App.css';
///Se importa Styled COmponents para darle más estilo a la tabla
import 'styled-components';
import React, { useState, useEffect } from 'react';
//IMporta el data table para mostrar los datos en las tablas
import DataTable from 'react-data-table-component';



const App = () => {
  /// COnfiguraciones de los hook
  const [animes, setAnimes] = useState([]);

  /// Se declara funcion y se adjunta la url de donde saldrán los datos a mostrar 
  const JIKAN_URL = 'https://api.jikan.moe/v4/anime?q=Kuroshitsuji&sfw';
  const showData = async () => {
    const response = await fetch(JIKAN_URL);
    const data = await response.json();
    console.log(data);
    //EXtrae los datos del anime de data
    setAnimes(data.data); 
  };

  useEffect(() => {
    showData();
  }, []);

  /// Se configura el nombre de la columna y de donde vendrá de la API para regresar los datos :3
  const columns = [

    ///Para mostrar el título de cada anime
    {
      name: 'Titulo',
      selector: row => row.title,
    },

    ///Para mostrar de que tipo se trata, si es anime, ova, especial o pelicula
    {
      name: 'Tipo',
      selector: row => row.type,
    },

    ///Muestra el numero de episodios
    {
      name: 'Episodios',
      selector: row => row.episodes,
    },
  ];

  ///Se muestran los datos de la tabla
  return (
    <div className="App">
      <img src = "https://weekendotaku.files.wordpress.com/2016/07/black_butler.png"/>
      <h1>Informacion de temporadas y especiales</h1>
      <DataTable
        columns={columns}
        data={animes}
        pagination
      />
    </div>
  );
};

export default App;